﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public GameObject Target;
    Transform TargetPosition;
    Transform Camera;
    public Vector3 Offset = new Vector3(0, -2.6f, 3);
	void Start () {
        TargetPosition = Target.transform;
        Camera = this.gameObject.transform;
	}
	
	// Update is called once per frame
	void Update () {
	Camera.position = TargetPosition.position - Offset;
        Camera.LookAt(Target.transform);    
	}
}
